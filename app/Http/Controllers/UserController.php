<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Gate;

class UserController extends Controller
{
    public function index()
    {
        if (Gate::denies('isAdmin')) {
            return response('Unauthorized!', 401);
        }
        return User::simplePaginate(10);
    }

    public function store(Request $request)
    {
        // TODO harus isAdmin
        if (Gate::denies('isAdmin')) {
            return response('Unauthorized!', 401);
        }
        try {
            $user = User::create([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'password' => Hash::make($request->input('password')),
                'isAdmin' => $request->input('isAdmin')
            ]);
        } catch (\Exception $e) {
            throw $e;
            return response('Cannot add user!', 400);
        }
        return response($user);
    }

    public function show($id)
    {
        // TODO harus admin
        if (Gate::denies('isAdmin')) {
            return response('Unauthorized!', 401);
        }
        try {
          $user = User::findOrFail($id);
        } catch (\Exception $e) {
            return response('Cannot get user!', 400);
        }
        return response($user);
    }
    
    public function update(Request $request, $id)
    {
        // TODO harus admin
        if (Gate::denies('isAdmin') && $request->user()->id !== (int)$id) {
            return response('Unauthorized!', 401);
        }
        try {
          $user = User::findOrFail($id);
          $user->name = $request->input('name');
          $user->email = $request->input('email');
          if(Gate::allows('isAdmin') && $request->user()->id !== (int)$id) {
              $user->isAdmin = $request->input('isAdmin');
          }
          if(!empty($request->input('password')) && !empty($request->input('newPassword'))) {
              if(Hash::check($request->input('password'), $user->password)) {
                  $user->password = Hash::make($request->input('newPassword'));
              }
          }
          $user->save();
        } catch (\Exception $e) {
            throw $e;
            return response('Cannot update user!', 400);
        }
        return response($user);
    }

    public function destroy(Request $request, $id)
    {
        // TODO harus admin
        if (Gate::denies('isAdmin')) {
            return response('Unauthorized!', 401);
        }
        if($request->user()->id == $id) return response('Cannot delete your account!', 400);
        try {
          $user = User::findOrFail($id);
          $user->delete();
          return response([]);
        } catch (\Exception $e) {
          return response('Cannot delete user!', 400);
        }
    }
}