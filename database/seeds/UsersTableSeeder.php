<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'name' => str_random(10),
            'email' => str_random(10).'@mailinator.com',
            'password' => Hash::make('test'),
            'isAdmin' => true,
        ]);

        DB::table('users')->insert([
            'name' => str_random(10),
            'email' => str_random(10).'@mailinator.com',
            'password' => Hash::make('test'),
            'isAdmin' => false,
        ]);
    }
}
