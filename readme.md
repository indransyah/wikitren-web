# WIKITREN

Sistem pencarian pesantren dibangun menggunakan Laravel v5.5 & React v16

## Kebutuhan
- PHP v7.1.*
- Node v8.*
- MySQL v5.6
- ElasticSearch v5

## Instalasi

- Install dependensi Laravel dengan `composer install`.
_[composer](https://getcomposer.org/) harus sudah diinstall_

- Install dependensi JavaScript dengan `npm install`

- Generate Laravel key `php artisan key:generate`

- Save as file `.env.example` menjadi `.env` dan sesuaikan konfigurasi database. _konfigurasi dengan prefix DB__

- Install Laravel passport `php artisan passport:install`. Catat Client secret token dari Client ID 2

- Save as file `resources/assets/js/config/index.example.js` menjadi `resources/assets/js/config/index.js` dan masukkan client secret pada `clientId`

- Buat table-table database `php artisan migrate`

- Generate JavaScript script dan aset `npm run prod`

- Jalankan Laravel `php artisan serve`

