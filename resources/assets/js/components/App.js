import React, { Component } from "react";
import ReactDOM from "react-dom";
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  withRouter
} from "react-router-dom";
import Cookies from "js-cookie";

import Search from "./Search";
import Profile from "./Profile";
import EditUser from "./EditUser";
import AddUser from "./AddUser";
import Users from "./Users";
import Login from "./Login";
import PrivateRoute from "./PrivateRoute";
import PublicRoute from "./PublicRoute";
import Ponpes from "./Ponpes";

export default class App extends Component {
  render() {
    return (
      <Router basename="/">
        <div>
          <PublicRoute path="/login" component={Login} />
          <PrivateRoute path="/" exact component={Search} />
          <PrivateRoute path="/profile" component={Profile} />
          <PrivateRoute path="/users" component={Users} />
          <PrivateRoute path="/user/:id" component={EditUser} />
          <PrivateRoute path="/user" exact component={AddUser} />
          <PrivateRoute path="/ponpes/:id" exact component={Ponpes} />
        </div>
      </Router>
    );
  }
}

if (document.getElementById("app")) {
  ReactDOM.render(<App />, document.getElementById("app"));
}
