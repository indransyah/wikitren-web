import React, { Component } from "react";
import Cookies from "js-cookie";
import { Redirect, withRouter } from "react-router-dom";
import config from "../config";

class Login extends Component {
  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
    this.state = {
      email: "",
      password: ""
    };
  }

  onSubmit(e, data) {
    e.preventDefault();
    let accessToken;
    axios
      .post("/oauth/token", {
        username: this.state.email,
        password: this.state.password,
        grant_type: "password",
        client_id: 2,
        client_secret: config.clientId
      })
      .then(res => {
        console.log(res);
        accessToken = res.data.access_token;
        return axios.get("/api/user/info", {
          headers: {
            Authorization: `Bearer ${accessToken}`
          }
        });
      })
      .then(res => {
        Cookies.set("id", res.data.id, { expires: 1 });
        Cookies.set("name", res.data.name, { expires: 1 });
        Cookies.set("access_token", accessToken, { expires: 1 });
        // this.setState({ redirectToReferrer: true });
        this.props.history.push("/");
      })
      .catch(err => {
        console.log(err);
        this.setState({ isError: true });
      });
  }

  render() {
    const accessToken = Cookies.get("access_token");
    // console.log('access_token', {accessToken})
    if (accessToken) return <Redirect to="/" />;

    return (
      <div style={{ margin: "auto", width: "50%" }}>
        {this.state.isError && (
          <div className="alert alert-danger" role="alert">
            Tidak dapat login, silakan cek username dan password Anda!
          </div>
        )}
        <div className="panel panel-default">
          <div className="panel-heading text-center">
            <h3 className="panel-title">Login</h3>
          </div>
          <div className="panel-body">
            <form className="form-horizontal" onSubmit={this.onSubmit}>
              <div className="form-group">
                <label htmlFor="inputEmail3" className="col-sm-2 control-label">
                  Email
                </label>
                <div className="col-sm-10">
                  <input
                    type="email"
                    className="form-control"
                    id="inputEmail3"
                    placeholder="Email"
                    onChange={e => this.setState({ email: e.target.value })}
                    value={this.state.email}
                  />
                </div>
              </div>
              <div className="form-group">
                <label
                  htmlFor="inputPassword3"
                  className="col-sm-2 control-label"
                >
                  Password
                </label>
                <div className="col-sm-10">
                  <input
                    type="password"
                    className="form-control"
                    id="inputPassword3"
                    placeholder="Password"
                    onChange={e => this.setState({ password: e.target.value })}
                    value={this.state.password}
                  />
                </div>
              </div>
              <div className="form-group">
                <div className="col-sm-offset-2 col-sm-10">
                  <button type="submit" className="btn btn-default">
                    Login
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(Login);
