import React, { Component } from "react";
import Cookies from "js-cookie";
import config from "../config";

export default class Ponpes extends Component {
  constructor(props) {
    super(props);
    this.state = {
        data: {}
    };
  }

  componentDidMount() {
    const {match} = this.props;
    const {params: {id}} = match;
    
    axios
      .get(config.elasticsearchUrl+"wikitren/ponpes/"+id)
      .then(res => {
        this.setState({
          data: res.data._source
        });
      })
      .catch(err => {
        console.log(err);
      });
  }

  formatCurrency(num) {
    return new Intl.NumberFormat("id-ID", {
      style: "currency",
      currency: "IDR"
    }).format(num);
  }
  
  render() {
    const {data} = this.state;
    
    return (
      <div
        className="panel panel-default"
        key={data.id}
        style={{ fontSize: "12px", marginBottom: "1rem" }}
      >
        <div className="panel-heading">
          <h4>
            {data.nama}
          </h4>
          <i>
            {data.kiai}, {data.kabupaten} ({data.tahun})
          </i>
          <br />
          {data.nomor}
        </div> 
        <div className="panel-body">
          <div className="text-center">
            <div className="btn-group">
              <button className="btn btn-primary" type="button">
                Ekonomi{" "}
                <span className="badge">
                  {data.gradeEkonomi}
                </span>
              </button>
              <button className="btn btn-primary" type="button">
                Politik{" "}
                <span className="badge">
                  {data.gradePolitik}
                </span>
              </button>
              <button className="btn btn-primary" type="button">
                Social{" "}
                <span className="badge">
                  {data.gradeSosial}
                </span>
              </button>
              <button className="btn btn-primary" type="button">
                Kelembagaan{" "}
                <span className="badge">
                  {data.gradeKelembagaan}
                </span>
              </button>
            </div>
          </div>
        </div>
        <div className="panel-body">
          <div className="row">
            <div className="col-sm-12">
              <strong>Yayasan : </strong>
              {data.yayasan}
              <br />
              <strong>Alamat : </strong>
              {data.jalan}, {data.kelurahan}, {data.kecamatan}
              <br />
              <strong>Ormas : </strong>
              {data.ormas}
              <br />
              <strong>Parpol : </strong>
              {data.parpol}
            </div>
          </div>
          <hr />
          <div className="row">
            <div className="col-md-6">
              <strong>Tipe : </strong>
              {data.tipe}
              <br />
              <strong>Ideologi : </strong>
              {data.ideologi}
              <br />

              <strong>Biaya Masuk : </strong>
              {this.formatCurrency(data.biaya_masuk)}
              <br />
              <strong>Biaya Bulanan : </strong>
              {this.formatCurrency(data.biaya_bulanan)}
              <br />
              <strong>Omset Perbulan : </strong>
              {this.formatCurrency(data.omset)}
              <br />
              <strong>Pengakuan : </strong>
              {(data.pengakuan || []).join(", ")}
              <br />
              <strong>Kepemilikan : </strong>
              {(data.pemilikan || []).join(", ")}
              <br />
              <strong>Potensi : </strong>
              {(data.potensi || []).join(", ")}
            </div>
            <div className="col-md-6">
              <strong>Luas : </strong>
              {data.luas}
              <br />
              <strong>Santri : </strong>
              {data.jumlah_santri} orang
              <br />
              <strong>Ustadz : </strong>
              {data.jumlah_ustadz} orang
              <br />
              <strong>Kitab Kuning : </strong>
              {data.kitab_kuning === 2 ? "Ya" : "Tidak"}
              <br />
              <strong>Bantuan Pemerintah : </strong>
              {data.bantuan_pemerintah ? "Sudah" : "Belum"}
              <br />
              <strong>Bantuan Non Pemerintah : </strong>
              {data.bantuan_non_pemerintah ? "Sudah" : "Belum"}
              <br />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
