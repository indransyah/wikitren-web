import React, { Component } from "react";
import { Route, Redirect, withRouter } from "react-router-dom";
import Cookies from "js-cookie";

class PrivateRoute extends Component {
  constructor(props) {
    super(props);
    this.onLogout = this.onLogout.bind(this);
  }

  onLogout() {
    const { history } = this.props;
    Cookies.remove("id");
    Cookies.remove("name");
    Cookies.remove("access_token");
    console.log("ups");
    history.push("/login");
  }

  render() {
    const { component: Component, ...props } = this.props;
    const name = Cookies.get("name");
    const accessToken = Cookies.get("access_token");

    return (
      <Route
        {...props}
        render={props =>
          !accessToken ? (
            <Redirect
              to={{
                pathname: "/login"
              }}
            />
          ) : (
            <div>
              <nav className="navbar navbar-default navbar-fixed-top">
                <div className="container">
                  <div className="navbar-header">
                    <a className="navbar-brand" href="/">
                      Wikitren
                    </a>
                  </div>
                  <div id="navbar">
                    <ul className="nav navbar-nav navbar-right">
                      <li className="dropdown">
                        <a
                          href="#"
                          className="dropdown-toggle"
                          data-toggle="dropdown"
                          role="button"
                          aria-haspopup="true"
                          aria-expanded="false"
                        >
                          {name} <span className="caret" />
                        </a>
                        <ul className="dropdown-menu">
                          <li>
                            <a href="/profile">Profil</a>
                          </li>
                          <li>
                            <a href="/users">Pengguna</a>
                          </li>
                          <li role="separator" className="divider" />
                          <li onClick={this.onLogout}>
                            <a href="#">Keluar</a>
                          </li>
                        </ul>
                      </li>
                    </ul>
                  </div>
                </div>
              </nav>
              <div className="container">
                <Component {...props} />
              </div>
            </div>
          )
        }
      />
    );
  }
}
export default withRouter(PrivateRoute);
