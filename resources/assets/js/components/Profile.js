import React, { Component } from "react";
import Cookies from "js-cookie";

export default class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: "",
      name: "",
      email: "",
      password: "",
      newPassword: "",
      isAdmin: false,
      isSuccess: false,
      isError: false
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }
  componentDidMount() {
    const accessToken = Cookies.get("access_token");
    axios
      .get("/api/user/info", {
        headers: {
          Authorization: `Bearer ${accessToken}`
        }
      })
      .then(res => {
        console.log(res.data);
        this.setState({
          id: res.data.id,
          name: res.data.name,
          email: res.data.email,
          isAdmin: res.data.isAdmin
        });
      })
      .catch(err => {
        console.log(err);
      });
  }
  onChange(inputName, value) {
    this.setState({
      [inputName]: value
    });
  }
  onSubmit(e, data) {
    e.preventDefault();
    const accessToken = Cookies.get("access_token");
    this.setState({
      isError: false,
      isSuccess: false
    });
    axios
      .put(
        `/api/user/${this.state.id}`,
        {
          name: this.state.name,
          email: this.state.email,
          password: this.state.password,
          newPassword: this.state.newPassword,
          isAdmin: this.state.isAdmin
          // _method: 'PUT'
        },
        {
          headers: {
            Authorization: `Bearer ${accessToken}`
          }
        }
      )
      .then(res => {
        console.log(res);
        this.setState({
          isSuccess: true
        });
        // accessToken = res.data.access_token;
        // return axios.get("/api/user/info", {
        //   headers: {
        //     Authorization: `Bearer ${accessToken}`
        //   }
        // });
      })
      .then(res => {
        // Cookies.set("id", res.data.id, { expires: 1 });
        // Cookies.set("name", res.data.name, { expires: 1 });
        // Cookies.set("access_token", accessToken, { expires: 1 });
        // // this.setState({ redirectToReferrer: true });
        // this.props.history.push("/");
      })
      .catch(err => {
        console.log(err);
        this.setState({ isError: true });
      });
  }
  render() {
    return (
      <div>
        {this.state.isError && (
          <div className="alert alert-danger" role="alert">
            Ups, terjadi kesalahan!
          </div>
        )}
        {this.state.isSuccess && (
          <div className="alert alert-success" role="alert">
            Data berhasil disunting!
          </div>
        )}
        <div className="panel panel-default">
          <div className="panel-heading">
            <h3 className="panel-title">Profil</h3>
          </div>
          <div className="panel-body">
            <form onSubmit={this.onSubmit}>
              <div className="form-group">
                <label htmlFor="exampleInputName">Nama</label>
                <input
                  type="text"
                  className="form-control"
                  id="exampleInputName"
                  placeholder="Nama"
                  value={this.state.name}
                  onChange={e => this.onChange("name", e.target.value)}
                />
              </div>
              <div className="form-group">
                <label htmlFor="exampleInputEmail1">Email</label>
                <input
                  type="email"
                  className="form-control"
                  id="exampleInputEmail1"
                  placeholder="Email"
                  value={this.state.email}
                  onChange={e => this.onChange("email", e.target.value)}
                />
              </div>
              <div className="form-group">
                <label htmlFor="exampleInputPassword1">Password</label>
                <input
                  type="password"
                  className="form-control"
                  id="exampleInputPassword1"
                  placeholder="Password"
                  value={this.state.password}
                  onChange={e => this.onChange("password", e.target.value)}
                />
              </div>

              <div className="form-group">
                <label htmlFor="exampleInputPassword2">New Password</label>
                <input
                  type="password"
                  className="form-control"
                  id="exampleInputPassword2"
                  placeholder="Password"
                  value={this.state.newPassword}
                  onChange={e => this.onChange("newPassword", e.target.value)}
                />
              </div>

              <div className="checkbox">
                <label>
                  <input
                    type="checkbox"
                    value={this.state.isAdmin}
                    onChange={e => console.log(e.target.checked)}
                  />{" "}
                  Tambah sebagai admin
                </label>
              </div>
              <button type="submit" className="btn btn-default">
                Submit
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
