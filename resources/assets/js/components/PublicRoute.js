import React, { Component } from "react";
import { Route, Redirect, withRouter } from "react-router-dom";
import Cookies from "js-cookie";

class PublicRoute extends Component {
  render() {
    const { component: Component, ...props } = this.props;
    const accessToken = Cookies.get("access_token");

    return (
      <Route
        {...props}
        render={props =>
          accessToken ? (
            <Redirect
              to={{
                pathname: "/"
              }}
            />
          ) : (
            <Component {...props} />
          )
        }
      />
    );
  }
}
export default withRouter(PublicRoute);
