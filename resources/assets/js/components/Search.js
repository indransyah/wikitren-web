import {
  DataSearch,
  RangeInput,
  ReactiveBase,
  ReactiveList,
  SelectedFilters,
  SingleDropdownList,
  SingleList,
  MultiList,
  ToggleButton,
  NumberBox
} from "@appbaseio/reactivesearch";
import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import gradeEkonomi from "../libs/grade/ekonomi";
import gradeKelembagaan from "../libs/grade/lembaga";
import gradePolitik from "../libs/grade/politik";
import gradeSosial from "../libs/grade/sosial";
import config from "../config";

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      provinsi: null,
      kabupaten: null,
      kecamatan: null,
      pengakuan: null,
      omset: null,
      isOpen: false,
      temp: false,

      filters: [],
      values: {}
    };
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  formatCurrency(num) {
    return new Intl.NumberFormat("id-ID", {
      style: "currency",
      currency: "IDR"
    }).format(num);
  }

  filter(id) {
    const index = this.state.filters.findIndex(v => v === id);
    if(!!~index) {
      this.setState({filters: [
        ...this.state.filters.slice(0, index),
        ...this.state.filters.slice(index + 1)
      ]});
    } else {
      this.setState({filters: [...this.state.filters, id]})
    }
  }

  getDisplayStyle(id) {
    return {display: this.state.filters.includes(id) ? '' : 'none'}
  }

  render() {
    return (
      <ReactiveBase app="wikitren" type="ponpes" url={config.elasticsearchUrl}>
        
        <div className="container">
          <div className="row">
            <div className="col-sm-4 filters" style={{ fontSize: "12px" }}>
            <div className="panel panel-default">
                <div className="panel-heading" onClick={() => this.filter('alamat')} >Alamat
                  <span className={`glyphicon glyphicon-chevron-${this.state.filters.includes('alamat') ? 'up' : 'down'} pull-right arrow`} aria-hidden="true"></span>                  
                </div>
                <div className="panel-body" style={this.getDisplayStyle('alamat')}>
                  <MultiList
                    componentId="Provinsi"
                    dataField="provinsi.keyword"
                    title="Provinsi"
                    // showRadio={false}
                    // react={{
                    //   and: ["Ideologi", "Tipe", "Tahun"]
                    // }}
                    onValueChange={function(value) {
                      this.setState({
                        provinsi: value
                      });
                    }.bind(this)}
                  />
                  {!!(this.state.provinsi || []).length && (<React.Fragment><hr/><MultiList
                      componentId="Kabupaten"
                      dataField="kabupaten.keyword"
                      title="Kabupaten"
                      // showRadio={false}
                      react={{
                        // and: ["Ideologi", "Tipe", "Tahun", "Provinsi"]
                        and: ["Provinsi"]
                      }}
                      onValueChange={function(value) {
                        this.setState({
                          kabupaten: value
                        });
                      }.bind(this)}
                    /></React.Fragment>)}
                  {!!(this.state.kabupaten || []).length && (<React.Fragment><hr/><MultiList
                      componentId="Kecamatan"
                      dataField="kecamatan.keyword"
                      title="Kecamatan"
                      react={{
                        // and: ["Ideologi", "Tipe", "Tahun", "Kabupaten"]
                        and: ["Kabupaten"]
                      }}
                      // showRadio={false}
                      onValueChange={function(value) {
                        this.setState({
                          kecamatan: value
                        });
                      }.bind(this)}
                    /></React.Fragment>)}
                    {!!(this.state.kabupaten || []).length && !!(this.state.kecamatan || []).length && (<React.Fragment><hr/><MultiList
                      componentId="Kelurahan"
                      dataField="kelurahan.keyword"
                      title="Kelurahan"
                      react={{
                        // and: ["Ideologi", "Tipe", "Tahun", "Kecamatan"]
                        and: ["Kecamatan"]
                      }}
                      // showRadio={false}
                    /></React.Fragment>)}
                </div>
              </div>

              <div className="panel panel-default">
                <div className="panel-heading" onClick={() => this.filter('profile')}>Profil
                  <span 
                  className={`glyphicon glyphicon-chevron-${this.state.filters.includes('profile') ? 'up' : 'down'} pull-right arrow`} aria-hidden="true"></span>
                </div>
                <div className="panel-body" style={this.getDisplayStyle('profile')}>
                  <ToggleButton
                    componentId="BerbadanHukum"
                    dataField="berbadan_hukum"
                    title="Berbadan Hukum"
                    data={[
                      { label: "Ya", value: 1 },
                      { label: "Tidak", value: 0 }
                    ]}
                    multiSelect={false}
                  />
                  <hr/>
                  <NumberBox
                    componentId="GradeEkonomi"
                    dataField="gradeEkonomi"
                    data={{ "label": "Grade Ekonomi", "start": 0, "end": 5 }}
                    defaultSelected={0}
                    labelPosition="left"
                    queryFormat="gte"
                    URLParams={false}
                  />
                  <NumberBox
                    componentId="GradePolitik"
                    dataField="gradePolitik"
                    data={{ "label": "Grade Politik", "start": 0, "end": 5 }}
                    defaultSelected={0}
                    labelPosition="left"
                    queryFormat="gte"
                    URLParams={false}
                  />
                  <NumberBox
                    componentId="GradeSosial"
                    dataField="gradeSosial"
                    data={{ "label": "Grade Sosial", "start": 0, "end": 5 }}
                    defaultSelected={0}
                    labelPosition="left"
                    queryFormat="gte"
                    URLParams={false}
                  />
                  <NumberBox
                    componentId="GradeKelembagaan"
                    dataField="gradeKelembagaan"
                    data={{ "label": "Grade Kelembagaan", "start": 0, "end": 5 }}
                    defaultSelected={0}
                    labelPosition="left"
                    queryFormat="gte"
                    URLParams={false}
                  />
                  <hr/>
                  <RangeInput
                    componentId="Tahun"
                    dataField="tahun"
                    title="Tahun"
                    range={{
                      start: 1920,
                      end: 2019
                    }}
                    rangeLabels={{
                      start: "1920",
                      end: "2019"
                    }}
                    showFilter={true}
                    stepValue={1}
                    showHistogram={true}
                    interval={2}
                    URLParams={false}
                  />
                  <hr/>
                  <SingleDropdownList
                    componentId="Tipe"
                    dataField="tipe.keyword"
                    title="Tipe"
                    placeholder="Pilih tipe"
                    react={{
                      and: [
                        "Ideologi",
                        "Tahun",
                        "Provinsi",
                        "Kabupaten",
                        "Kecamatan",
                        "Kelurahan"
                      ]
                    }}
                  />
                  <hr/>
                  <SingleDropdownList
                    componentId="Ideologi"
                    dataField="ideologi.keyword"
                    title="Ideologi"
                    placeholder="Pilih ideologi"
                    react={{
                      and: [
                        "Tipe",
                        "Tahun",
                        "Provinsi",
                        "Kabupaten",
                        "Kecamatan",
                        "Kelurahan"
                      ]
                    }}
                  />
                  <hr/>
                  <RangeInput
                    componentId="Luas"
                    dataField="luas"
                    title="Luas"
                    range={{
                      start: 0,
                      end: 10000
                    }}
                    rangeLabels={{
                      start: "0",
                      end: "10k"
                    }}
                    // showFilter={true}
                    // stepValue={1}
                    showHistogram={false}
                    // interval={1}
                    URLParams={false}
                  />
                  <hr/>
                  <ToggleButton
                    componentId="Cabang"
                    dataField="cabang"
                    title="Cabang"
                    data={[
                      { label: "Punya", value: 1 },
                      { label: "Tidak", value: 0 }
                    ]}
                    multiSelect={false}
                  />
                </div>
              </div>
              {/* <div className="panel panel-default">
                <div className="panel-heading"
                  onClick={() => this.filter('grade')} 
                >Grade
                  <span 
                  className={`glyphicon glyphicon-chevron-${this.state.filters.includes('grade') ? 'up' : 'down'} pull-right arrow`} aria-hidden="true"></span>
                </div>
                <div className="panel-body" style={this.getDisplayStyle('grade')}>
                  
                </div>
              </div> */}
              {/* {!!(this.state.provinsi || []).length && (
                <div className="panel panel-default">
                  <div className="panel-heading" onClick={() => this.filter('kabupaten')} >Kabupaten
                    <span className={`glyphicon glyphicon-chevron-${this.state.filters.includes('kabupaten') ? 'up' : 'down'} pull-right arrow`} aria-hidden="true"></span>
                  </div>
                  <div className="panel-body" style={this.getDisplayStyle('kabupaten')}>
                    
                  </div>
                </div>
              )}
              {!!(this.state.kabupaten || []).length && (
                <div className="panel panel-default">
                  <div className="panel-heading" onClick={() => this.filter('kecamatan')} >Kecamatan
                    <span className={`glyphicon glyphicon-chevron-${this.state.filters.includes('kecamatan') ? 'up' : 'down'} pull-right arrow`} aria-hidden="true"></span>
                  </div>
                  <div className="panel-body" style={this.getDisplayStyle('kecamatan')}>
                    
                  </div>
                </div>
              )}
              {!!(this.state.kabupaten || []).length && !!(this.state.kecamatan || []).length && (
                <div className="panel panel-default">
                  <div className="panel-heading" onClick={() => this.filter('kelurahan')} >Kelurahan
                    <span className={`glyphicon glyphicon-chevron-${this.state.filters.includes('kelurahan') ? 'up' : 'down'} pull-right arrow`} aria-hidden="true"></span>
                  </div>
                  <div className="panel-body" style={this.getDisplayStyle('kelurahan')}>
                    
                  </div>
                </div>
              )} */}

              {/* <div className="panel panel-default">
                <div className="panel-heading"
                  onClick={() => this.filter('tahun')} 
                >Tahun
                  <span 
                  className={`glyphicon glyphicon-chevron-${this.state.filters.includes('tahun') ? 'up' : 'down'} pull-right arrow`} aria-hidden="true"></span>
                </div>
                <div className="panel-body" style={this.getDisplayStyle('tahun')}>
                  
                </div>
              </div> */}
              
              {/* <div className="panel panel-default">
                <div className="panel-heading">Pendiri</div>
                <div className="panel-body">
                  <SingleList
                    componentId="Pendiri"
                    dataField="pendiri"
                    // title="Pendiri"
                    placeholder="Pendiri"
                    showRadio={false}
                  />
                </div>
              </div> */}

              {/* <div className="panel panel-default">
                <div className="panel-heading"
                  onClick={() => this.filter('tipe')} 
                >Tipe
                  <span 
                  className={`glyphicon glyphicon-chevron-${this.state.filters.includes('tipe') ? 'up' : 'down'} pull-right arrow`} aria-hidden="true"></span>
                </div>
                <div className="panel-body" style={this.getDisplayStyle('tipe')}>
                  
                </div>
              </div> */}

              {/* <div className="panel panel-default">
                <div className="panel-heading"
                  onClick={() => this.filter('ideologi')} 
                >Ideologi
                  <span 
                  className={`glyphicon glyphicon-chevron-${this.state.filters.includes('ideologi') ? 'up' : 'down'} pull-right arrow`} aria-hidden="true"></span>
                </div>
                <div className="panel-body" style={this.getDisplayStyle('ideologi')}>
                  
                </div>
              </div> */}
              {/* <div className="panel panel-default">
                <div className="panel-heading"
                  onClick={() => this.filter('kitabKuning')} 
                >Kitab Kuning
                  <span 
                  className={`glyphicon glyphicon-chevron-${this.state.filters.includes('kitabKuning') ? 'up' : 'down'} pull-right arrow`} aria-hidden="true"></span>
                </div>
                <div className="panel-body" style={this.getDisplayStyle('kitabKuning')}>
                  
                </div>
              </div> */}

              {/* <div className="panel panel-default">
                <div className="panel-heading"
                  onClick={() => this.filter('luas')} 
                >Luas
                  <span 
                  className={`glyphicon glyphicon-chevron-${this.state.filters.includes('luas') ? 'up' : 'down'} pull-right arrow`} aria-hidden="true"></span>
                </div>
                <div className="panel-body" style={this.getDisplayStyle('luas')}>
                  
                </div>
              </div> */}
              {/* <div className="panel panel-default">
                <div className="panel-heading"
                  onClick={() => this.filter('jumlahUstadz')} 
                >Jumlah Ustadz
                  <span 
                  className={`glyphicon glyphicon-chevron-${this.state.filters.includes('jumlahUstadz') ? 'up' : 'down'} pull-right arrow`} aria-hidden="true"></span>
                </div>
                <div className="panel-body" style={this.getDisplayStyle('jumlahUstadz')}>
                  {" "}
                  
                </div>
              </div>
              <div className="panel panel-default">
                <div className="panel-heading"
                  onClick={() => this.filter('jumlahSantri')} 
                >Jumlah Santri
                  <span 
                  className={`glyphicon glyphicon-chevron-${this.state.filters.includes('jumlahSantri') ? 'up' : 'down'} pull-right arrow`} aria-hidden="true"></span>
                </div>
                <div className="panel-body" style={this.getDisplayStyle('jumlahSantri')}>
                  
                </div>
              </div> */}
              <div className="panel panel-default">
                <div className="panel-heading" onClick={() => this.filter('akademik')}>Akademik
                  <span 
                  className={`glyphicon glyphicon-chevron-${this.state.filters.includes('akademik') ? 'up' : 'down'} pull-right arrow`} aria-hidden="true"></span>
                </div>
                <div className="panel-body" style={this.getDisplayStyle('akademik')}>
                  <ToggleButton
                    componentId="KitabKuning"
                    dataField="kitab_kuning"
                    title="Kitab Kuning"
                    data={[
                      { label: "Ya", value: 2 },
                      { label: "Tidak", value: 1 },
                      // { label: "Abstain", value: 0 }
                    ]}
                    multiSelect={false}
                  />
                  <hr/>
                  <RangeInput
                    componentId="JumlahUstadz"
                    dataField="jumlah_ustadz"
                    title="Jumlah Ustadz"
                    range={{
                      start: 0,
                      end: 100
                    }}
                    rangeLabels={{
                      start: "0",
                      end: "100"
                    }}
                    showFilter={true}
                    stepValue={1}
                    showHistogram={false}
                    interval={1}
                    URLParams={false}
                  />
                  <hr/>
                  <RangeInput
                    componentId="JumlahSantri"
                    dataField="jumlah_santri"
                    title="Jumlah Santri"
                    range={{
                      start: 0,
                      end: 1000
                    }}
                    rangeLabels={{
                      start: "0",
                      end: "1000"
                    }}
                    showFilter={true}
                    stepValue={1}
                    showHistogram={false}
                    interval={10}
                    URLParams={false}
                  />
                  <hr/>
                  <RangeInput
                    componentId="BiayaMasuk"
                    dataField="biaya_masuk"
                    title="Biaya Masuk"
                    range={{
                      start: 0,
                      end: 5000000
                    }}
                    rangeLabels={{
                      start: "0",
                      end: "5jt"
                    }}
                    showFilter={true}
                    stepValue={1}
                    showHistogram={false}
                    interval={50000}
                    URLParams={false}
                  />
                  <hr/>
                  <RangeInput
                    componentId="BiayaBulanan"
                    dataField="biaya_bulanan"
                    title="Biaya Bulanan"
                    range={{
                      start: 0,
                      end: 1000000
                    }}
                    rangeLabels={{
                      start: "0",
                      end: "1jt"
                    }}
                    showFilter={true}
                    stepValue={1}
                    showHistogram={false}
                    interval={10000}
                    URLParams={false}
                  />
                  <hr/>
                  <MultiList
                    componentId="Pemilikan"
                    dataField="pemilikan"
                    title="Pemilikan"
                    placeholder="Pemilikan"
                    // showRadio={false}
                  />
                </div>
              </div>
              {/* <div className="panel panel-default">
                <div className="panel-heading"
                  onClick={() => this.filter('biayaBulanan')} 
                >Biaya Bulanan
                  <span 
                  className={`glyphicon glyphicon-chevron-${this.state.filters.includes('biayaBulanan') ? 'up' : 'down'} pull-right arrow`} aria-hidden="true"></span>
                </div>
                <div className="panel-body" style={this.getDisplayStyle('biayaBulanan')}>
                  
                </div>
              </div> */}
              {/* <div className="panel panel-default">
                <div className="panel-heading"
                  onClick={() => this.filter('kepemilikan')} 
                >Kepemilikan
                  <span 
                  className={`glyphicon glyphicon-chevron-${this.state.filters.includes('kepemilikan') ? 'up' : 'down'} pull-right arrow`} aria-hidden="true"></span>
                </div>
                <div className="panel-body" style={this.getDisplayStyle('kepemilikan')}>
                  
                </div>
              </div> */}
              {/* <div className="panel panel-default">
                <div className="panel-heading"
                  onClick={() => this.filter('pengajian')} 
                >Pengajian
                  <span 
                  className={`glyphicon glyphicon-chevron-${this.state.filters.includes('pengajian') ? 'up' : 'down'} pull-right arrow`} aria-hidden="true"></span>
                </div>
                <div className="panel-body" style={this.getDisplayStyle('pengajian')}>
                  
                </div>
              </div> */}
              <div className="panel panel-default">
                <div className="panel-heading" onClick={() => this.filter('ekonomi')}>Ekonomi
                  <span 
                  className={`glyphicon glyphicon-chevron-${this.state.filters.includes('ekonomi') ? 'up' : 'down'} pull-right arrow`} aria-hidden="true"></span>
                </div>
                <div className="panel-body" style={this.getDisplayStyle('ekonomi')}>
                  <SingleDropdownList
                    className="dropdown-field"
                    componentId="UnitUsaha"
                    dataField="status_omset.keyword"
                    title="Unit Usaha"
                    placeholder="Apakah memiliki unit usaha"
                    onValueChange={value => {
                      this.setState({
                        omset: value === "Punya" ? true : false
                      });
                    }}
                  />
                  {this.state.omset && (<React.Fragment><hr/><RangeInput
                    componentId="Omset"
                    dataField="omset"
                    title="Omset"
                    range={{
                      start: 0,
                      end: 10000000
                    }}
                    rangeLabels={{
                      start: "0",
                      end: "10jt"
                    }}
                    showFilter={true}
                    stepValue={1}
                    showHistogram={false}
                    interval={2}
                    URLParams={false}
                  /></React.Fragment>)}
                  <hr/>
                  <SingleList
                  componentId="Potensi"
                  dataField="potensi"
                  title="Potensi"
                  placeholder="Potensi"
                  showRadio={false}
                />
                </div>
              </div>
              {/* {this.state.omset && (
                <div className="panel panel-default">
                  <div className="panel-heading"
                    onClick={() => this.filter('omset')} 
                  >Omset
                    <span 
                    className={`glyphicon glyphicon-chevron-${this.state.filters.includes('omset') ? 'up' : 'down'} pull-right arrow`} aria-hidden="true"></span>
                  </div>
                  <div className="panel-body" style={this.getDisplayStyle('omset')}>
                    
                  </div>
                </div>
              )} */}
              {/* <div className="panel panel-default">
                <div className="panel-heading"
                  onClick={() => this.filter('potensi')} 
                >Potensi
                  <span 
                  className={`glyphicon glyphicon-chevron-${this.state.filters.includes('potensi') ? 'up' : 'down'} pull-right arrow`} aria-hidden="true"></span>
                </div>
                <div className="panel-body" style={this.getDisplayStyle('potensi')}>
                  
                </div>
              </div> */}
              {/* <div className="panel panel-default">
                <div className="panel-heading"
                  onClick={() => this.filter('cabang')} 
                >Cabang
                  <span 
                  className={`glyphicon glyphicon-chevron-${this.state.filters.includes('cabang') ? 'up' : 'down'} pull-right arrow`} aria-hidden="true"></span>
                </div>
                <div className="panel-body" style={this.getDisplayStyle('cabang')}>
                  
                </div>
              </div> */}
              {/* <div className="panel panel-default">
                <div className="panel-heading"
                  onClick={() => this.filter('ormas')} 
                >Ormas
                  <span 
                  className={`glyphicon glyphicon-chevron-${this.state.filters.includes('ormas') ? 'up' : 'down'} pull-right arrow`} aria-hidden="true"></span>
                </div>
                <div className="panel-body" style={this.getDisplayStyle('ormas')}>
                  
                </div>
              </div> */}
              {/* <div className="panel panel-default">
                <div className="panel-heading"
                  onClick={() => this.filter('parpol')} 
                >Parpol
                  <span 
                  className={`glyphicon glyphicon-chevron-${this.state.filters.includes('parpol') ? 'up' : 'down'} pull-right arrow`} aria-hidden="true"></span>
                </div>
                <div className="panel-body" style={this.getDisplayStyle('parpol')}>
                  
                </div>
              </div> */}
              <div className="panel panel-default">
                <div className="panel-heading" onClick={() => this.filter('sosial')}>Sosial
                  <span 
                  className={`glyphicon glyphicon-chevron-${this.state.filters.includes('sosial') ? 'up' : 'down'} pull-right arrow`} aria-hidden="true"></span>
                </div>
                <div className="panel-body" style={this.getDisplayStyle('sosial')}>
                  <SingleList
                    componentId="Ormas"
                    dataField="ormas.keyword"
                    title="Ormas"
                    placeholder="Afiliasi Ormas"
                    showRadio={false}
                    react={{
                      and: [
                        "Tipe",
                        "Tahun",
                        "Provinsi",
                        "Kabupaten",
                        "Kecamatan",
                        "Kelurahan"
                      ]
                    }}
                  />
                  <hr/>
                  <ToggleButton
                    componentId="BantuanPemerintah"
                    dataField="bantuan_pemerintah"
                    title="Bantuan Pemerintah"
                    data={[
                      { label: "Sudah", value: 1 },
                      { label: "Belum", value: 0 }
                    ]}
                    multiSelect={false}
                  />
                  <hr/>
                  <ToggleButton
                    componentId="BantuanNonPemerintah"
                    dataField="bantuan_non_pemerintah"
                    title="Bantuan Non Pemerintah"
                    data={[
                      { label: "Sudah", value: 1 },
                      { label: "Belum", value: 0 }
                    ]}
                    multiSelect={false}
                  />
                  <hr/>
                  <ToggleButton
                    componentId="Acara"
                    dataField="acara"
                    title="Acara"
                    data={[
                      { label: "Pernah", value: 1 },
                      { label: "Belum Pernah", value: 0 }
                    ]}
                    multiSelect={false}
                  />
                  <hr/>
                  <RangeInput
                    componentId="Pengajian"
                    dataField="pengajian"
                    title="Pengajian"
                    range={{
                      start: 0,
                      end: 5000
                    }}
                    rangeLabels={{
                      start: "0",
                      end: "5K"
                    }}
                    showFilter={true}
                    stepValue={1}
                    showHistogram={false}
                    interval={50}
                    URLParams={false}
                  />
                  <hr/>
                  <SingleDropdownList
                    componentId="StatusPengakuan"
                    dataField="status_pengakuan.keyword"
                    title="Status Pengakuan"
                    placeholder="Pilih Pengakuan"
                    onValueChange={value => {
                      this.setState({
                        pengakuan: value === "Sudah" ? true : false
                      });
                    }}
                  />
                  {this.state.pengakuan !== null && this.state.pengakuan && (<React.Fragment><hr/><RangeInput
                      componentId="JumlahPengakuan"
                      dataField="jumlah_pengakuan"
                      title="Jumlah Pengakuan"
                      range={{
                        start: 0,
                        end: 20
                      }}
                      rangeLabels={{
                        start: "0",
                        end: "20"
                      }}
                      showFilter={true}
                      stepValue={1}
                      showHistogram={true}
                      interval={1}
                      URLParams={false}
                    /><hr/><DataSearch
                    componentId="Pengakuan"
                    dataField={["pengakuan", "pengakuan.search"]}
                    categoryField="pengakuan.keyword"
                    placeholder="Cari berdasarkan nama"
                    style={{
                      marginBottom: 20
                    }}
                    title="Pengakuan"
                    fieldWeights={[1, 3]}
                    autoSuggest={true}
                    react={{
                      and: ["JumlahPengakuan"]
                    }}
                  /></React.Fragment>)}
                </div>
              </div>
              {/* <div className="panel panel-default">
                <div className="panel-heading"
                  onClick={() => this.filter('bantuanNonPemerintah')} 
                >Bantuan Non Pemerintah
                  <span 
                  className={`glyphicon glyphicon-chevron-${this.state.filters.includes('bantuanNonPemerintah') ? 'up' : 'down'} pull-right arrow`} aria-hidden="true"></span>
                </div>
                <div className="panel-body" style={this.getDisplayStyle('bantuanNonPemerintah')}>
                  
                </div>
              </div> */}
              {/* <div className="panel panel-default">
                <div className="panel-heading"
                  onClick={() => this.filter('statusPengakuan')} 
                >Status Pengakuan
                  <span 
                  className={`glyphicon glyphicon-chevron-${this.state.filters.includes('statusPengakuan') ? 'up' : 'down'} pull-right arrow`} aria-hidden="true"></span>
                </div>
                <div className="panel-body" style={this.getDisplayStyle('statusPengakuan')}>
                  
                </div>
              </div> */}
              {/* {this.state.pengakuan !== null && this.state.pengakuan && (
                <React.Fragment>
                  <div className="panel panel-default">
                    <div className="panel-heading"
                      onClick={() => this.filter('jumlahPengakuan')} 
                    >Jumlah Pengakuan
                      <span 
                      className={`glyphicon glyphicon-chevron-${this.state.filters.includes('jumlahPengakuan') ? 'up' : 'down'} pull-right arrow`} aria-hidden="true"></span>
                    </div>
                    <div className="panel-body" style={this.getDisplayStyle('jumlahPengakuan')}>
                      
                    </div>
                  </div>
                  <div className="panel panel-default">
                    <div className="panel-heading"
                      onClick={() => this.filter('pengakuan')} 
                    >Nama Tokoh
                      <span 
                      className={`glyphicon glyphicon-chevron-${this.state.filters.includes('pengakuan') ? 'up' : 'down'} pull-right arrow`} aria-hidden="true"></span>
                    </div>
                    <div className="panel-body" style={this.getDisplayStyle('pengakuan')}>
                      
                    </div>
                  </div>
                </React.Fragment>
              )} */}
              {/* <div className="panel panel-default">
                <div className="panel-heading"
                  onClick={() => this.filter('acara')} 
                >Penyelengara Acara Besar
                  <span 
                  className={`glyphicon glyphicon-chevron-${this.state.filters.includes('acara') ? 'up' : 'down'} pull-right arrow`} aria-hidden="true"></span>
                </div>
                <div className="panel-body" style={this.getDisplayStyle('acara')}>
                  
                </div>
              </div> */}
              <div className="panel panel-default">
                <div className="panel-heading" onClick={() => this.filter('politik')}>Politik
                  <span 
                  className={`glyphicon glyphicon-chevron-${this.state.filters.includes('politik') ? 'up' : 'down'} pull-right arrow`} aria-hidden="true"></span>
                </div>
                <div className="panel-body" style={this.getDisplayStyle('politik')}>
                  <SingleList
                    componentId="Parpol"
                    dataField="parpol.keyword"
                    title="Parpol"
                    placeholder="Afiliasi Parpol"
                    showRadio={false}
                    react={{
                      and: [
                        "Tipe",
                        "Tahun",
                        "Provinsi",
                        "Kabupaten",
                        "Kecamatan",
                        "Kelurahan"
                      ]
                    }}
                  />
                  <hr/>
                  <ToggleButton
                    componentId="Pilpres"
                    dataField="pilpres"
                    title="Pilpres"
                    data={[
                      { label: "01", value: 1 },
                      { label: "02", value: 2 },
                      { label: "Netral", value: 0 }
                    ]}
                    multiSelect={false}
                  />
                </div>
              </div>
              {/*  */}
            </div>
            <div className="col-sm-8">
              <DataSearch
                componentId="Cari"
                dataField={[
                  "kiai",
                  "nomor",
                  "yayasan",
                  "nama",
                  "nama.autosuggest",
                  "nama.search",
                  "yayasan.autosuggest",
                  "yayasan.keyword",
                  "yayasan.search",
                  "nomor.autosuggest",
                  "nomor.search",
                  "kiai.autosuggest",
                  "kiai.keyword",
                  "kiai.search"
                ]}
                fieldWeights={[7, 7, 7, 7, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]}
                fuzziness={0}
                highlightField={["nama", "yayasan", "nomor", "kiai"]}
                placeholder="Cari berdasarkan nama, yayasan, nomor, atau nama kiai"
                style={{
                  marginBottom: 20
                }}
                title="Cari"
              />
              <SelectedFilters clearAllLabel="Hapus semua filter" />

              <ReactiveList
                componentId="result"
                dataField="nama"
                pagination={true}
                react={{
                  and: [
                    "Cari",
                    "BerbadanHukum",
                    "Kabupaten",
                    "Kecamatan",
                    "Kelurahan",
                    "Tahun",
                    "Luas",
                    "Jumlah Ustadz",
                    "Jumlah Santri",
                    "Pilpres",
                    "Ideologi",
                    "Tipe",
                    "Luas",
                    "JumlahUstadz",
                    "JumlahSantri",
                    "BiayaMasuk",
                    "BiayaBulanan",
                    "BantuanPemerintah",
                    "BantuanNonPemerintah",
                    "Pendiri",
                    "KitabKuning",
                    "Cabang",
                    "Acara",
                    "Pengakuan",
                    "Pengajian",
                    "JumlahPengakuan",
                    "StatusPengakuan",
                    "UnitUsaha",
                    "Omset",
                    "Potensi",
                    "Pemilikan",

                    "GradeEkonomi",
                    "GradePolitik",
                    "GradeSosial",
                    "GradeKelembagaan",
                  ]
                }}
                onData={res => {
                  return (
                    <div
                      className="panel panel-default"
                      key={res.id}
                      style={{ fontSize: "12px", marginBottom: "1rem" }}
                    >
                      <div className="panel-heading">
                        <h4>
                          <a href={`/ponpes/${res.id}`}>{res.nama}</a>
                        </h4>
                        <i>
                          {res.kiai}, {res.kabupaten} ({res.tahun})
                        </i>
                        <br />
                        {res.nomor}
                      </div>
                      <div className="panel-body">
                        {/* <span>
                          <h4>{res.nama}</h4>
                          <i>{res.kiai}</i>
                        </span> */}
                        <div className="text-center">
                          <div className="btn-group">
                            <button className="btn btn-primary" type="button">
                              Ekonomi{" "}
                              <span className="badge">
                                {res.gradeEkonomi}
                              </span>
                            </button>
                            <button className="btn btn-primary" type="button">
                              Politik{" "}
                              <span className="badge">
                                {res.gradePolitik}
                              </span>
                            </button>
                            <button className="btn btn-primary" type="button">
                              Social{" "}
                              <span className="badge">
                                {res.gradeSosial}
                              </span>
                            </button>
                            <button className="btn btn-primary" type="button">
                              Kelembagaan{" "}
                              <span className="badge">
                                {res.gradeKelembagaan}
                              </span>
                            </button>
                          </div>
                        </div>
                      </div>
                      <div className="panel-body">
                        <div className="row">
                          <div className="col-sm-12">
                            <strong>Yayasan : </strong>
                            {res.yayasan}
                            <br />
                            <strong>Alamat : </strong>
                            {res.jalan}, {res.kelurahan}, {res.kecamatan}
                            <br />
                            <strong>Ormas : </strong>
                            {res.ormas}
                            <br />
                            <strong>Parpol : </strong>
                            {res.parpol}
                          </div>
                        </div>
                        {/*
                        <hr />
                        <div className="row">
                          <div className="col-md-6">
                            <strong>Tipe : </strong>
                            {res.tipe}
                            <br />
                            <strong>Ideologi : </strong>
                            {res.ideologi}
                            <br />

                            <strong>Biaya Masuk : </strong>
                            {this.formatCurrency(res.biaya_masuk)}
                            <br />
                            <strong>Biaya Bulanan : </strong>
                            {this.formatCurrency(res.biaya_bulanan)}
                            <br />
                            <strong>Omset Perbulan : </strong>
                            {this.formatCurrency(res.omset)}
                            <br />
                            <strong>Pengakuan : </strong>
                            {res.pengakuan.join(", ")}
                            <br />
                            <strong>Kepemilikan : </strong>
                            {res.pemilikan.join(", ")}
                            <br />
                            <strong>Potensi : </strong>
                            {res.potensi.join(", ")}
                          </div>
                          <div className="col-md-6">
                            <strong>Luas : </strong>
                            {res.luas}
                            <br />
                            <strong>Santri : </strong>
                            {res.jumlah_santri} orang
                            <br />
                            <strong>Ustadz : </strong>
                            {res.jumlah_ustadz} orang
                            <br />
                            <strong>Kitab Kuning : </strong>
                            {res.kitab_kuning === 2 ? "Ya" : "Tidak"}
                            <br />
                            <strong>Bantuan Pemerintah : </strong>
                            {res.bantuan_pemerintah ? "Sudah" : "Belum"}
                            <br />
                            <strong>Bantuan Non Pemerintah : </strong>
                            {res.bantuan_non_pemerintah ? "Sudah" : "Belum"}
                            <br />
                          </div>
                        </div>
                        */}
                      </div>
                    </div>
                  );
                }}
                size={10}
                style={{
                  marginTop: 20
                }}
              />
            </div>
          </div>
        </div>
      </ReactiveBase>
    );
  }
}

export default withRouter(Search);
