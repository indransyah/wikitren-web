import React, { Component } from "react";
import Cookies from "js-cookie";
import { withRouter } from "react-router-dom";

class Users extends Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      users: []
    };
    this.onDelete = this.onDelete.bind(this);
    this.onEdit = this.onEdit.bind(this);
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevState.page !== this.state.page) {
      console.log("Tidak", prevState.page, this.state.page);
      const accessToken = Cookies.get("access_token");
      axios
        .get(`/api/user/?page=${this.state.page}`, {
          headers: {
            Authorization: `Bearer ${accessToken}`
          }
        })
        .then(res => {
          console.log(res.data);
          this.setState({
            users: res.data.data
          });
        })
        .catch(err => {
          console.log(err);
        });
    }
  }

  componentDidMount() {
    const accessToken = Cookies.get("access_token");
    axios
      .get(`/api/user/?page=${this.state.page}`, {
        headers: {
          Authorization: `Bearer ${accessToken}`
        }
      })
      .then(res => {
        console.log(res.data);
        this.setState({
          users: res.data.data
        });
      })
      .catch(err => {
        console.log(err);
      });
  }

  onDelete(id) {
    const accessToken = Cookies.get("access_token");
    axios
      .delete(`/api/user/${id}`, {
        headers: {
          Authorization: `Bearer ${accessToken}`
        }
      })
      .then(res => {
        console.log(res.data);
        window.location.reload();
      })
      .catch(err => {
        console.log(err);
      });
  }
  onEdit(id) {
    this.props.history.push(`/user/${id}`);
  }
  render() {
    return (
      <div className="panel panel-default">
        <div className="panel-heading">
          <h3 className="panel-title">Profil</h3>
        </div>
        <div className="panel-body">
          <button
            type="button"
            className="btn btn-success pull-right"
            aria-label="Tambah data"
            style={{ marginBottom: 5 }}
            onClick={() => this.props.history.push(`/user`)}
          >
            <span className="glyphicon glyphicon-plus" aria-hidden="true" />{" "}
            Tambah
          </button>
          <table className="table table-bordered">
            <thead>
              <tr>
                <th>#</th>
                <th>Nama</th>
                <th>Email</th>
                <th>Admin</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              {this.state.users.map(user => (
                <tr key={user.id}>
                  <th scope="row">{user.id}</th>
                  <td>{user.name}</td>
                  <td>{user.email}</td>
                  <td>{user.isAdmin ? 'Ya' : 'Tidak'}</td>
                  <td className="text-center">
                    <div className="btn-group">
                      <button
                        type="button"
                        className="btn btn-default"
                        aria-label="Sunting data"
                        onClick={e => {
                          this.onEdit(user.id);
                        }}
                      >
                        <span
                          className="glyphicon glyphicon-pencil"
                          aria-hidden="true"
                        />{" "}
                        Sunting
                      </button>
                      <button
                        type="button"
                        className="btn btn-danger"
                        aria-label="Hapus data"
                        onClick={e => {
                          const confirmation = confirm(
                            "Apakah Anda yakin akan menghapus data ini?"
                          );
                          if (confirmation) this.onDelete(user.id);
                        }}
                      >
                        <span
                          className="glyphicon glyphicon-trash"
                          aria-hidden="true"
                        />{" "}
                        Hapus
                      </button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
          <nav aria-label="...">
            <ul className="pager">
              <li className="previous">
                <a
                  href="#"
                  onClick={e =>
                    this.setState({
                      page: this.state.page > 1 ? this.state.page - 1 : 1
                    })
                  }
                >
                  <span aria-hidden="true">&larr;</span> Sebelumnya
                </a>
              </li>
              <li className="next">
                <a
                  href="#"
                  onClick={e => this.setState({ page: this.state.page + 1 })}
                >
                  Selanjutnya <span aria-hidden="true">&rarr;</span>
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    );
  }
}

export default withRouter(Users);
