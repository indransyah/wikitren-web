export const gradeBiayaMasuk = biaya_masuk => {
  let bobot = 3;
  if (biaya_masuk > 0 && biaya_masuk <= 1e6) bobot = 2;
  if (biaya_masuk > 1e6 && biaya_masuk <= 3e6) bobot = 3;
  if (biaya_masuk > 3e6 && biaya_masuk <= 6e6) bobot = 4;
  if (biaya_masuk > 6e6) bobot = 5;
  return bobot;
};

export const gradeBiayaBulanan = biaya_bulanan => {
  let bobot = 3;
  if (biaya_bulanan > 0 && biaya_bulanan <= 5e5) bobot = 2;
  if (biaya_bulanan > 5e5 && biaya_bulanan <= 1e6) bobot = 3;
  if (biaya_bulanan > 1e6 && biaya_bulanan <= 2e6) bobot = 4;
  if (biaya_bulanan > 2e6) bobot = 5;
  return bobot;
};

export const gradeJumlahSantri = jumlah_santri => {
  let bobot = 1;
  if (jumlah_santri > 1e2 && jumlah_santri <= 1e3) bobot = 3;
  if (jumlah_santri > 1e3 && jumlah_santri <= 5e3) bobot = 5;
  if (jumlah_santri > 5e3 && jumlah_santri <= 1e4) bobot = 7;
  if (jumlah_santri > 1e4) bobot = 9;
  return bobot;
};

export const gradeOmset = omset => {
  let bobot = 0;
  if (omset > 0 && omset <= 2e7) bobot = 1;
  if (omset > 2e7 && omset <= 5e7) bobot = 2;
  if (omset > 5e7) bobot = 3;
  return bobot;
};

const gradeEkonomi = ({ biaya_masuk, biaya_bulanan, jumlah_santri, omset }) => {
  const bobot =
    gradeBiayaMasuk(biaya_masuk) +
    gradeBiayaBulanan(biaya_bulanan) +
    gradeJumlahSantri(jumlah_santri) +
    gradeOmset(omset);
  let grade = 0;
  if (bobot <= 6) grade = 1;
  if (bobot >= 7 && bobot <= 9) grade = 2;
  if (bobot >= 10 && bobot <= 11) grade = 3;
  if (bobot >= 12 && bobot <= 13) grade = 4;
  if (bobot > 13) grade = 5;
  return grade;
};

export default gradeEkonomi;
