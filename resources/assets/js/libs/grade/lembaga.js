const gradePemilikan = pemilikan => {
  let bobot = 0;
  switch (pemilikan) {
    case "PAUD/ Taman Kanak-Kanak":
      bobot = 1;
      break;
    case "Sekolah Dasar (SD)":
      bobot = 2;
      break;
    case "Madrasah Ibtidaiyah (MI)":
      bobot = 2;
      break;
    case "Sekolah Menengah Pertama (SMP)":
      bobot = 3;
      break;
    case "Madrasah Tsanawiyah (MTs)":
      bobot = 3;
      break;
    case "Sekolah Menengah Atas (SMA)":
      bobot = 4;
      break;
    case "Madrasah Aliyah (MA)":
      bobot = 4;
      break;
    case "Sekolah Menengah Kejuruan(SMK) ":
      bobot = 4;
      break;
    case "Sekolah Tinggi":
      bobot = 5;
      break;
    case "Institut":
      bobot = 5;
      break;
    case "Politeknik":
      bobot = 5;
      break;
    case "Universitas":
      bobot = 6;
      break;
    case "Ma’had Aly":
      bobot = 5;
      break;
    case "Pendidikan Diniyah Formal":
      bobot = 3;
      break;
    case "Pendidikan Muadalah":
      bobot = 4;
      break;
    case "Madrasah Diniyah":
      bobot = 1;
      break;
    case "Taman Pendidikan Quran (TPQ/TPA)":
      bobot = 1;
      break;
  }
  return bobot;
};

const gradeJumlahMuridPerUnit = jumlah => {
  let bobot = 0;
  if (jumlah < 100) bobot = 1;
  if (jumlah > 100 && jumlah <= 300) bobot = 2;
  if (jumlah > 300) bobot = 3;
  return bobot;
};

const gradeKelembagaan = ({ pemilikan = [], jumlah_murid_per_unit = [] }) => {
  let bobot = 0;
  for (let i = 0; i < pemilikan.length; i++) {
    bobot =
      bobot +
      gradePemilikan(pemilikan[i]) +
      gradeJumlahMuridPerUnit(jumlah_murid_per_unit[i]);
  }
  let grade = 0;
  if (bobot < 5) grade = 1;
  if (bobot >= 6 && bobot <= 8) grade = 2;
  if (bobot >= 9 && bobot <= 11) grade = 3;
  if (bobot >= 12 && bobot <= 15) grade = 4;
  if (bobot > 15) grade = 5;
  return grade;
};

export default gradeKelembagaan;
