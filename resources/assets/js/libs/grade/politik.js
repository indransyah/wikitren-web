import { gradeJumlahSantri } from "./ekonomi";
export const gradeJamaah = (jumlah_jamaah = 0) => {
  let bobot = 0;
  if (jumlah_jamaah > 0 && jumlah_jamaah <= 1e2) bobot = 1;
  if (jumlah_jamaah > 1e2 && jumlah_jamaah <= 5e2) bobot = 2;
  if (jumlah_jamaah > 5e2 && jumlah_jamaah <= 1e3) bobot = 3;
  if (jumlah_jamaah > 1e3) bobot = 4;
  return bobot;
};

export const gradeTokoh = (tokoh = []) => {
  let bobot = 0;
  if (tokoh.length > 0) bobot = tokoh.length * 3;
  return bobot;
};

const gradePolitik = ({
  jumlah_santri,
  jumlah_jamaah,
  ormas,
  parpol,
  tokoh
}) => {
  const bobotOrmas = ormas ? 1 : 0;
  const bobotParpol = parpol ? 1 : 0;
  const bobot =
    gradeJumlahSantri(jumlah_santri) +
    gradeJamaah(jumlah_jamaah) +
    bobotOrmas +
    bobotParpol +
    gradeTokoh(tokoh);
  let grade = 0;
  if (bobot > 0 && bobot <= 3) grade = 1;
  if (bobot >= 4 && bobot <= 6) grade = 2;
  if (bobot >= 7 && bobot <= 9) grade = 3;
  if (bobot >= 10 && bobot <= 12) grade = 4;
  if (bobot > 12) grade = 5;
  return grade;
};

export default gradePolitik;
