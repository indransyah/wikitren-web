const gradeSosial = ({
  jabatan,
  bantuanPemerintah,
  bantuanNonPemerintah,
  acara,
  ormas
}) => {
  const bobotBantuanPemerintah = !!bantuanPemerintah ? 3 : 0;
  const bobotBantuanNonPemerintah = !!bantuanNonPemerintah ? 1 : 0;
  const bobotAcara = !!acara ? 3 : 1;
  const bobotOrmas = ormas ? 1 : 0;
  let bobot =
    jabatan +
    bobotBantuanPemerintah +
    bobotBantuanNonPemerintah +
    bobotAcara +
    bobotOrmas;
  let grade = 0;
  if (bobot <= 1) grade = 1;
  if (bobot >= 2 && bobot <= 3) grade = 2;
  if (bobot >= 4 && bobot <= 5) grade = 3;
  if (bobot >= 6 && bobot <= 7) grade = 4;
  if (bobot >= 8 && bobot <= 9) grade = 5;
  return grade;
};

export default gradeSosial;
