<html>
    <head>
        <title>WIKITREN</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link href="/css/app.css" rel="stylesheet" type="text/css">
        <link href="/css/non-responsive.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="app"></div>
        <!-- <script src="/js/manifest.js"></script> -->
        <!-- <script src="/js/vendor.js"></script> -->
        <script src="/js/app.js"></script>
    </body>
</html>